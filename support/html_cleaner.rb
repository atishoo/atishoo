require 'sanitize'
require 'nokogiri'

class HtmlCleaner

  def self.allow_basic_html html
    basic_sanitizer.clean html
  end

  def self.convert_to_plain html
    doc = Nokogiri::HTML.parse html
    doc.css("br").each { |node| node.replace("\n") }
    doc.text
  end

  private
  
  def self.basic_sanitizer
    @@basic_sanitizer ||= Sanitize.new( Sanitize::Config::BASIC )
  end
end
