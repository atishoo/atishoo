
class UnknownOptionsError < ArgumentError
  def initialize unused_opts = {}
    super "Unused parameters supplied: #{unused_opts.keys.join(', ')}"
  end
end
