
class Flash

  def initialize session
    @session = session
  end
  
  def message type, msg
    @session[:flash] ||= []
    @session[:flash] << [ type, msg ]
  end

  def error msg
    # Using standard Twitter Bootstrap css class.
    message :error, msg
  end

  def warning msg
    # Using standard Twitter Bootstrap css class.
    message :info, msg
  end

  def notice msg
    # Using standard Twitter Bootstrap css class.
    message :success, msg
  end

  def markup do_empty=true
    @session[:flash] ||= []

    markup = @session[:flash].inject('') do | markup, (type, msg) |
      "#{markup}<div class=\"alert alert-#{type.to_s}\"><span>#{msg}</span></div>"
    end

    @session.delete :flash if do_empty

    markup
  end
end
