require 'mail'


class Notifier

  def initialize url_scheme, email_address_scheme
    @url_scheme = url_scheme
    @email_address_scheme = email_address_scheme
  end

  def notify_about_comment comment, recipients
    
    users = recipients.respond_to?(:each) ? recipients : [recipients]

    users.each do |u|
      # Note: Do not set the constant fields on a template and then duplicate
      # in order fill out the variable fields. That fails, at least in testing.
      mail = Mail.new
      mail.from = @email_address_scheme.notification_from_address
      mail.to = u.email
      mail.subject = "New comment for '#{comment.issue.title}'"
      mail.body = comment_notification_body( comment )
      mail.deliver!
    end
  end

  private

  def comment_notification_body comment
    <<-ENDBODY
      #{comment.user.login} commented on '#{comment.issue.title}':


#{comment.plain_text_body}

      You can see this comment at: #{@url_scheme.comment_url(comment)}
    ENDBODY
  end
end
