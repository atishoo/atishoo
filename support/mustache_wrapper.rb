
module MustacheWrappper
  
  def mustache view, locals = {}

    if locals.has_key? :layout
      layout = locals.delete( :layout )
    else
      layout = :layout
    end
    
    rendered = render_view view, locals

    if layout
      render_view layout, locals.merge( {:yield => rendered} )
    else
      rendered
    end
  end

  
  def render_view view, locals

    # Is the view a Mustache-derived class or the name of one?
    view_instance =
      if view === Mustache
        view.new locals
      else
        get_view_instance_from_name view, locals
      end

    if view_instance
      view_instance.render locals
    else
      # Assume that view is the name of a template.
      Mustache.render view, locals
    end
  end

  
  def get_view_instance_from_name name, locals
    
    view_class = Kernel.const_get name rescue nil

    unless view_class
      camel_case_name = name.to_s.split(/[\W_]/).map {|c| c.capitalize}.join
      view_class = Kernel.const_get camel_case_name rescue nil
    end

    view_class.new( locals ) if view_class
  end
  
end
