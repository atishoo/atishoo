
class UrlScheme

  def initialize host, scheme='http://'
    @host = host
    @scheme = scheme
  end

  def comment_url comment
    "#{@scheme}#{@host}/issue/#{comment.issue.id}"
  end
end
