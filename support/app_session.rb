
class AppSession

  def initialize user_class, session
    @user_class = user_class
    @session = session
  end
  
  def current_user
    @current_user ||= @user_class.get( @session[:user_id] )
  end

  def logged_in?
    !!current_user
  end

  def log_in user
    self.current_user = user
  end

  def log_out
    self.current_user = nil
  end

  private
  
  def current_user= user
    if user.is_a? @user_class
      @session[:user_id] = user.id
      @current_user = user
    else
      @session[:user_id] = user
      @current_user = nil
    end
  end
end
