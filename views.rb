
require 'views/layout.rb'

require 'views/project_list.rb'
require 'views/issue_list.rb'
require 'views/view_issue.rb'
require 'views/add_issue.rb'
require 'views/logged_out_home.rb'
require 'views/user_settings.rb'
