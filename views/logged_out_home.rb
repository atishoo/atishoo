
class LoggedOutHome < Mustache

  attr_reader :projects
  
  def initialize locals
    super()
    locals[:page_name] = "Home"

    @projects = Project.all
  end
end
