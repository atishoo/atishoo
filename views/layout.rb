require 'mustache'

class Layout < Mustache

  attr_reader :app_name, :meta_title, :user_name, :scripts_markup

  def self.app_name
    @@app_name ||= "No app name set!"
  end

  def self.app_name= app_name
    @@app_name = app_name
  end
  
  def initialize locals
    @app_name = locals[:app_name] || Layout.app_name
    @meta_title = get_meta_title locals, @app_name
    @user_name = locals[:current_user].login if locals[:current_user]
    @scripts_markup = get_scripts_markup locals[:scripts]
  end

  private
  
  def get_meta_title locals, app_name
    if locals[:meta_title]
      locals[:meta_title]
    else
      suffix = locals[:meta_title_suffix]
      "#{app_name}#{' - ' if app_name and suffix}#{suffix}"
    end
  end
  
  def get_scripts_markup scripts
    scripts.inject("") do | markup, script |
      if script.start_with? '/'
        "#{markup}<script src=\"#{script}\"></script>"
      else
        "#{markup}<script type=\"text/javascript\">#{script}</script>"
      end
    end
  end

end
