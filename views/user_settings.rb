
class UserSettings < Mustache

  attr_reader :app_name
  
  def initialize locals
    super()
    locals[:page_name] = "Settings"
    locals[:app_name] = Layout.app_name
  end
end
