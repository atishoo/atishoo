
class ViewIssue < Mustache

  attr_reader :is_active_member

  def initialize locals
    super()

    issue = locals[:issue]
    user = locals[:current_user]

    locals[:meta_title_suffix] = issue.title
    locals[:project] = issue.project
    locals[:page_name] = issue.title

    locals[:scripts] << "/bootstrap-wysihtml5-0.0.2/libs/js/wysihtml5-0.3.0_rc2.min.js"
    locals[:scripts] << "/bootstrap-wysihtml5-0.0.2/bootstrap-wysihtml5-0.0.2.min.js"
    locals[:scripts] << "$('#description-text').wysihtml5();"

    @is_active_member = user && user.is_active_member_of?( issue.project )
  end
end
