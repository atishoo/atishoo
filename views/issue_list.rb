
class IssueList < Mustache

  attr_reader :assigned_issues, :discussed_issues, :other_issues
  attr_reader :may_add_issues, :may_subscribe, :is_subscribed
  

  def initialize locals
    super()
    
    project = locals[:project]
    user = locals[:current_user]
    
    locals[:meta_title_suffix] = project.title
    locals[:project_name] = project.title
    locals[:page_name] = "Issue list"

    if user
      # !!! HACK - Added this to get the set arithmetic correct - HACK !!!
      # !!! HACK - See https://github.com/datamapper/dm-core/issues/263 HACK !!!
      user.discussed_issues.inspect
      
      @assigned_issues = user.assigned_issues & project.issues
      @discussed_issues = ( user.issues & project.issues ) - @assigned_issues
      @other_issues = project.issues - ( @discussed_issues | @assigned_issues )
      @may_add_issues = user.is_active_member_of? project
      @is_subscribed = user.is_subscribed_to? project

    else
      @other_issues = project.issues
    end
    
    # There may be constraints on whether a user may subscribe to
    # projects but by always showing subscription UI, user may try,
    # fail and (hopefully) learn what they need in order to subscribe.
    @may_subscribe = true
  end

end
