
class ProjectList < Mustache

  attr_reader :other_projects
  
  def initialize locals
    super()
    locals[:page_name] = "Project list"

    user = locals[:current_user]

    @other_projects = Project.all - user.projects
  end
end
