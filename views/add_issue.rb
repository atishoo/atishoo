
class AddIssue < Mustache

  def initialize locals
    super()

    project = locals[:project]

    locals[:meta_title_suffix] = "New issue for #{project.title}"
    locals[:page_name] = 'New issue'

    locals[:scripts] << "/bootstrap-wysihtml5-0.0.2/libs/js/wysihtml5-0.3.0_rc2.min.js"
    locals[:scripts] << "/bootstrap-wysihtml5-0.0.2/bootstrap-wysihtml5-0.0.2.min.js"
    locals[:scripts] << "$('#description-text').wysihtml5();"
  end
end
