require 'sinatra/base'
require 'dm-core'
require 'dm-migrations'
require 'rack'
require 'mustache'
require 'omniauth'
require 'omniauth-twitter'

# Add the path of the main app file to the load path
$LOAD_PATH.unshift( File.expand_path( File.dirname( __FILE__ ) ) )

require 'support/unknown_options_error.rb'
require 'support/app_session.rb'
require 'support/flash.rb'
require 'support/mustache_wrapper.rb'
require 'support/notifier.rb'
require 'support/url_scheme.rb'
require 'support/email_address_scheme.rb'

require 'controllers.rb'


class Atishoo < Sinatra::Base
  include ::MustacheWrappper

  
  def initialize opts = {}
    # This class is the only app. Nothing is downstream of here.
    super nil
    
    self.class.init_sinatra opts
    init_datamapper opts
    init_app opts
    raise UnknownOptionsError.new(opts) unless opts.empty?
  end

  
  def self.init_sinatra opts
    use Rack::Session::Cookie, {
      :key => opts.delete(:session_key),
      :secret => opts.delete(:session_secret)
    }
    
    OmniAuth.config.on_failure = Proc.new { |env|
      OmniAuth::FailureEndpoint.new(env).redirect_to_failure
    }

    use OmniAuth::Strategies::Twitter, opts.delete(:twitter_key), opts.delete(:twitter_secret)

    configure do
      set :raise_errors, true
      set :show_exceptions, false
      
      # Some startup methods work without this but other startup methods fail
      # to find the public directory (css, js, etc), hence setting :root here.
      set :root, opts.delete( :root_directory )
    end
  end

  
  def init_datamapper opts = {}
    db_type = opts.delete :db_type

    case db_type
    when :sqlite3
      db_name = opts.delete :db_name
      dm_setup_string = "sqlite3://#{db_name}"
      DataMapper.setup(:default, dm_setup_string )

    when :mysql
      db_name = opts.delete :db_name
      user_name = opts.delete :db_user
      password = opts.delete :db_password
      host = opts.delete :db_host
      DataMapper.setup( :default,
                        "mysql://#{user_name}:#{password}@#{host}/#{db_name}" )

    else
      raise ArgumentError.new( "Unknown database type: #{db_type}" )
    end
    
    DataMapper::Model.raise_on_save_failure
    DataMapper.finalize
  end

  
  def init_app opts
    Layout.app_name = opts.delete :app_name

    # Mustache does not refer to the load path so we must set the template
    # path relevant to the current working directory.
    Mustache.template_path = "#{File.dirname( __FILE__ )}/views"
  end
    

  def app_session
    @app_session ||= AppSession.new( User, session )
  end

  def flash
    @flash ||= Flash.new( session )
  end

  def notifier
    host = request.host
    host = "#{host}:#{request.port}" unless request.port == 80

    email_opts = { :notification_from_address => 'justin@3mules.com' }
    
    @notifier ||= Notifier.new( UrlScheme.new(host),
                                EmailAddressScheme.new( email_opts ) )
  end


  # Add some application-specific locals before calling the mustache wrapper.
  #
  def mustache view, locals={}
    locals[:scripts] ||= []
    locals[:css] ||= []
    locals[:flash] = flash.markup
    locals[:current_user] = app_session.current_user
    super
  end
end
