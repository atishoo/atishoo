require_relative '../../support/app_session.rb'


describe AppSession do

  before :each do
    @user_logins = ['candy', 'herbie']

    class TestUser
      def self.set_users users; @@users = users; end
      def self.get index; @@users[index] if index.is_a? Fixnum; end
    end
    TestUser.set_users( @user_logins.map { |u| double( :login => u ) } )
    
    @sut = AppSession.new TestUser, { user_id: 0 }
  end

  it "should return the current user" do
    @sut.current_user.login.should eq( @user_logins[0] )
  end

  it "should log out the current user" do
    @sut.log_out
    @sut.current_user.should be_nil
    @sut.logged_in?.should be_false
  end

  it "should log in a new current user" do
    @sut.log_in 1
    @sut.current_user.login.should eq( @user_logins[1] )
    @sut.logged_in?.should be_true
  end

end
