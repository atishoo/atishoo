require_relative '../../support/flash.rb'

describe Flash do

  it "should echo a message that it was given" do
    sut = Flash.new({})
    
    sut.notice "Foo bar."
    sut.markup.include? "Foo bar."

    sut.warning "Anitdisestablishmentarianism"
    markup = sut.markup
    markup.include? "Anitdisestablishmentarianism"

    sut.error "Froo froo la rue"
    sut.markup.include? "Froo froo la rue"

    sut.message :fruity, "Froo froo la rue"
    markup = sut.markup
    markup.include? "Froo froo la rue"
    markup.include? "fruity"
  end
end
