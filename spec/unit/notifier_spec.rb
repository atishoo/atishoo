require_relative '../../support/notifier.rb'


describe "Notifier" do
  include Mail::Matchers
  
  before :all do
    Mail.defaults do
      delivery_method :test
    end
  end
  
  before :each do
    @logins = ['brandy', 'bertie']
    
    @users = @logins.map do |u|
      double( :login => u, :email => email_address(u) )
    end

    @emails = @users.map { |u| u.email }

    Mail::TestMailer.deliveries.clear
  end

  
  it "should notify relevant users about a new comment" do
    comment = double( :plain_text_body => "When I was holy as a house.",
                      :id => 3123,
                      :issue => double( :id => 635,
                                        :title => "yoyoyo" ),
                      :user => double( :login => 'frufru' ) )
    
                      
    url_scheme = double( :comment_url => "some.kind/of/url" )
    email_scheme = double( :notification_from_address => "them@there" )
    
    sut = Notifier.new( url_scheme, email_scheme )
    sut.notify_about_comment comment, @users

    @emails.each { |e| should have_sent_email.to(e).matching_subject( /#{comment.issue.title}/ ) }
    
    # Matching breaks when I add this for the body.
    # Need another way to test the body.
    # .matching_body( /#{comment.filtered_body}.*#{url_scheme.comment_url}/ ) }
  end


  private

  def email_address name
    "#{name}@example.com"
  end
    
end
