
describe 'Something' do

  before :each do
    @stuff = double( 'book', :x => 1 )
  end

  it 'should do something' do
    @stuff.x.should eq(1)
  end
end
