require_relative 'support/test_data.rb'
require_relative 'support/integration_test_helpers.rb'


describe 'User account tests', :type => :feature do
  include IntegrationTestHelpers
  
  before :each do
    # This does not work in before :all
    # Is there some setting needed to preserve the session between tests?
    # Is it as bad idea to have assertions in before :each ?
    sign_in TestData.primary_user[:name], TestData.primary_user[:password]
  end


  it "should be able to change own email address." do
    page.visit "/user/#{TestData.primary_user[:id]}/settings"
    page.should have_content "Email address"
    find_field('email').value.should eq TestData.primary_user[:email]

    test_email_address = 'testing@setting.email'

    fill_in 'Email address', with: test_email_address
    page.click_on 'Save'

    page.should have_content "Email address"
    find_field('email').value.should eq test_email_address
  end
end
