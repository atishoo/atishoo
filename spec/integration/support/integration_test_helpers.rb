
module IntegrationTestHelpers

  def sign_in username, password
    page.visit '/admin/login'

    page.should have_content 'Username'
    page.should have_content 'Password'
    page.should have_content 'Sign in'

    page.fill_in 'Username', :with => username
    page.fill_in 'Password', :with => password
    page.click_button 'Sign in'

    page.should have_content 'Sign out'
    page.should have_content 'Welcome'
    page.should have_content username
  end

  def add_issue_to_current_project title, text=nil, assignee=nil
    page.click_on 'New issue'

    page.fill_in( 'issue-title', :with => title ) if title
    page.fill_in( 'description-text', :with => text ) if text
    page.choose( assignee ) if assignee
    page.click_button 'Submit'

    page.should have_content( title ) if title
    page.should have_content( text ) if text
    page.should have_content( "Assigned to: #{assignee}" ) if assignee
  end

  def add_comment_to_current_issue text, assignee = nil
    page.fill_in( 'description-text', :with => text ) if text
    page.choose( assignee ) if assignee
    page.click_button 'Submit'

    page.should have_content( text )
    page.should have_content( "Assigned to: #{assignee}" ) if assignee
  end

  def validate_projects_list projects
    page.should have_content 'Project list'
    page.should have_content 'Your projects'
    page.should have_content 'Other projects'

    projects.each do |p|
      page.should have_content p[:title]
    end

    all( '.projects-where-member li' ).each do | el |
      el.should_not have_content 'Join'
    end

    all( '.other-projects li' ).each do | el |
      el.should have_content 'Join'
    end
  end
  
  def validate_project_page project
    page.should have_content project[:title]
    page.should have_content 'Issue list'
    page.should have_content 'Your assigned issues'
    project[:issues].each do |i|
      page.should have_content i[:title]
    end
  end

  def validate_issue_page issue, project=nil
    page.should have_content( project[:title] ) if project
    page.should have_content issue[:title]

    issue[:comments].each do |c|
      page.should have_content c[:text]
    end
  end

end
