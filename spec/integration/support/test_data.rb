require "sqlite3"
require 'capybara/rspec'
require 'capybara/poltergeist'

require_relative '../../../atishoo.rb'


module TestData

  def self.memberships
    @@memberships ||=
      [
       { user:1, project:21 },
       { user:2, project:22 },
       { user:2, project:23 },
       { user:3, project:23 },
       { user:3, project:21 }
      ]
  end

  def self.projects
    @@projects ||=
      [
       {
         id:21, friendly_id:'crazy_1', title:'One crazy project', status: :private,
         issues:
         [
          { id:11, title:'Where are my widgets', assignee:1, project:21,
            comments: [ { id:31, text:"I'm not convinced there were any widgets", user:1 } ] },
          { id:12, title:'Who took my pen', project:21,
            comments: [ { id:32, text:"If there were pen round ere there're no pen now", user:1 } ] }
         ]
       },
       {
         id:22, friendly_id:'less_crazy_1', title:'An only mildly crazy project', status: :public,
         issues:
         [
          { id:13, title:'Why are you always like that', project:22,
            comments:[ {id:33, text:'Tra-la-la la-la', user:2 } ] }
         ]
       },
       {
         id:23, friendly_id:'nearly_sane_1', title:'A most sensible project', status: :private, issues:[]
       }
      ].map do |p|
      p[:url] = "/project/#{p[:id]}"
      p[:users] =  memberships.select{|m|m[:project]==p[:id]}.map{|m|m[:user]}
      p[:issues].each { |i| i[:url] = "/issue/#{i[:id]}" }
      p
    end
  end
  
  def self.users
    @@users ||=
      [
       {id:1, name:'miranda', password:'password_1'},
       {id:2, name:'bertie', password:'password_2'},
       {id:3, name:'sandy'}
      ].map do |u|
      u[:email] = "#{u[:name]}@example.com"
      u[:projects] = memberships.select{|m|m[:user]==u[:id]}.map{|m|m[:project]}
      u
    end
  end

  def self.public_project
    p = self.projects[1]
    throw "Public test project is not public!" unless p[:status] == :public
    p
  end

  def self.primary_user
    users.first
  end
end 


module TestTarget

  def self.create_db

    db_name = Dir.pwd + "/test_data_setup_1.db"

    # Delete any existing database
    `rm #{db_name}`

    # Create a database
    db = SQLite3::Database.new( db_name )

    db.execute <<-ENDSQL
      CREATE TABLE "users" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "login" VARCHAR(63) NOT NULL, "email" VARCHAR(63));
    ENDSQL
    TestData.users.map { |u| [ u[:id], u[:name], u[:email] ] }.each do |x|
      db.execute "insert into users values (?,?,?);", x
    end

    db.execute <<-ENDSQL
      CREATE TABLE "projects" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "friendly_id" VARCHAR(31) NOT NULL, "title" VARCHAR(63) NOT NULL, "status" INTEGER DEFAULT 1 NOT NULL);
    ENDSQL
    projects = TestData.projects.map do |p|
      [ p[:id], p[:friendly_id], p[:title], (p[:status]==:public ? 2 : 1) ]
    end
    projects.each do | project |
      db.execute "insert into projects values (?, ?, ?, ?);", project
    end
    
    db.execute <<-ENDSQL
      CREATE TABLE "project_memberships" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "status" INTEGER DEFAULT 1 NOT NULL, "member_id" INTEGER NOT NULL, "project_id" INTEGER NOT NULL);
    ENDSQL
    TestData.memberships.map{ |m| [nil,2,m[:user],m[:project]] }.each do |m|
      db.execute "insert into project_memberships values (?, ?, ?, ?);", m
    end

    db.execute <<-ENDSQL
      CREATE TABLE "issues" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "title" VARCHAR(127) NOT NULL, "assigned_to_id" INTEGER, "project_id" INTEGER NOT NULL);
    ENDSQL
    TestData.projects.inject([]) do |issues,p|
      issues.concat( p[:issues].map { |i| [ i[:id], i[:title], i[:assignee], i[:project] ] } )
    end.each do | issue |
      db.execute "insert into issues values (?, ?, ?, ?);", issue
    end
    
    db.execute <<-ENDSQL
      CREATE TABLE "issue_comments" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "body" TEXT NOT NULL, "issue_id" INTEGER NOT NULL, "user_id" INTEGER NOT NULL);
    ENDSQL
    TestData.projects.inject([]) do |comments, p|
      comments.concat( p[:issues].inject([]) do |coms,i|
                         coms.concat( i[:comments].map do |c|
                                        [ c[:id], c[:text], i[:id], c[:user] ]
                                      end )
                       end )
    end .each do | comment |
      db.execute "insert into issue_comments values (?, ?, ?, ?);", comment
    end
                                                                       
    db.execute <<-ENDSQL
      CREATE TABLE "authorisations" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "provider" VARCHAR(31) NOT NULL, "uid" VARCHAR(63) NOT NULL, "user_id" INTEGER NOT NULL);
    ENDSQL
    
    db.execute <<-ENDSQL
      CREATE TABLE "admin_user_details" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "password" VARCHAR(60), "user_id" INTEGER NOT NULL);   
    ENDSQL
    
    db.execute <<-ENDSQL
      CREATE TABLE "pending_membership_details" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "issue_id" INTEGER NOT NULL, "project_membership_id" INTEGER NOT NULL);
    ENDSQL

    db.execute <<-ENDSQL
      CREATE TABLE "project_subscriptions" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "user_id" INTEGER NOT NULL, "project_id" INTEGER NOT NULL);
20,21d18
    ENDSQL
    
    db.execute <<-ENDSQL
      CREATE UNIQUE INDEX "unique_pending_membership_details_issue_id" ON "pending_membership_details" ("issue_id");
    ENDSQL

    db.execute <<-ENDSQL
      CREATE UNIQUE INDEX "unique_pending_membership_details_project_membership_id" ON "pending_membership_details" ("project_membership_id");
    ENDSQL

    db.execute <<-ENDSQL
      CREATE UNIQUE INDEX "unique_project_memberships_uniqueness" ON "project_memberships" ("member_id", "project_id");
    ENDSQL

    db.execute <<-ENDSQL
      CREATE UNIQUE INDEX "unique_project_subscriptions_uniqueness" ON "project_subscriptions" ("user_id", "project_id");
    ENDSQL
    
    db.execute <<-ENDSQL
      CREATE INDEX "index_authorisations_user" ON "authorisations" ("user_id");
    ENDSQL
    
    db.execute <<-ENDSQL
      CREATE UNIQUE INDEX "unique_authorisations_auth_index" ON "authorisations" ("provider", "uid");
    ENDSQL
    
    db.execute <<-ENDSQL
      CREATE INDEX "index_admin_user_details_user" ON "admin_user_details" ("user_id");  
    ENDSQL
    
    db.execute <<-ENDSQL
      CREATE INDEX "index_issues_assigned_to" ON "issues" ("assigned_to_id");
    ENDSQL

    db.execute <<-ENDSQL
      CREATE INDEX "index_issues_project" ON "issues" ("project_id");   
    ENDSQL

    db.execute <<-ENDSQL
      CREATE INDEX "index_issue_comments_issue" ON "issue_comments" ("issue_id");
    ENDSQL

    db.execute <<-ENDSQL
      CREATE INDEX "index_issue_comments_user" ON "issue_comments" ("user_id");
    ENDSQL

    {
      db_type: :sqlite3,
      db_name: db_name,
    }
  end


  def self.init_local_app
    puts
    puts "Starting database setup..."
    
    config = create_db
    puts "    #{config[:db_name]}"

    config[:session_key] = 'test_setup_1'
    config[:session_secret] = 'test_setup_1'
    config[:app_name] = 'atishoo(test)'
    
    Capybara.app = Atishoo.new( config )
    
    TestData.users.each do |u|
      if u[:password]
        AdminUserDetails.create( user: User.get(u[:id]), password: u[:password] )
      end
    end
    
    puts "...finished database setup"
    puts
  end


  def self.init_remote_app test_target
    Capybara.app_host = test_target
    Capybara.default_driver = :poltergeist
  end

  
  def self.init    
    if ENV['TEST_TARGET'] # testing a remote URL
      init_remote_app ENV['TEST_TARGET']
    else # testing locally
      init_local_app
    end
  end
end


# This should only be executed the first time this file is required.
TestTarget.init
