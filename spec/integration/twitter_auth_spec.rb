require_relative 'support/test_data.rb'


describe "Twitter Auth", :type => :feature do

  it "should exercise all twitter sign-in logic paths" do

    OmniAuth.config.test_mode = true

    page.visit '/logout'
    page.should have_content 'Goodbye'

    page.should_not have_content 'Join'

    OmniAuth.config.mock_auth[:twitter] =
      OmniAuth::AuthHash.new({
                               :provider => 'twitter',
                               :uid => '123545',
                               :info => {
                                 :nickname => 'shorty',
                                 :image => 'localhost/img/default-user.png'
                               }
                             })
    
    page.click_on 'Sign in with Twitter'
    page.should have_content 'Would you like to create an account'
    
    page.click_on 'Create account'
    page.should have_content 'Welcome to your new account'

    page.click_on 'Sign out'
    page.should have_content 'Goodbye'

    page.click_on 'Sign in with Twitter'
    page.should have_content 'Welcome'
    page.should_not have_content 'Welcome to your new account'
    
    page.click_on 'Sign out'
    page.should have_content 'Goodbye'

    OmniAuth.config.mock_auth[:twitter] =
      OmniAuth::AuthHash.new({
                               :provider => 'twitter',
                               :uid => '23232',
                               :info => {
                                 :nickname => 'crumbly',
                                 :image => 'localhost/img/default-user.png'
                               }
                             })
    
    page.click_on 'Sign in with Twitter'
    page.should have_content 'Would you like to create an account'

    page.click_on 'Oops! No thanks'
    page.should have_content 'Account creation cancelled'
  end

end
