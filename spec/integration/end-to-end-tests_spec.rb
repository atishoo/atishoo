require_relative 'support/test_data.rb'
require_relative 'support/integration_test_helpers.rb'


describe 'End-to-end tests', :type => :feature do
  include IntegrationTestHelpers
  
  before :each do
    # This does not work in before :all
    # Is there some setting needed to preserve the session between tests?
    # Is it as bad idea to have assertions in before :each ?
    sign_in TestData.primary_user[:name], TestData.primary_user[:password]
  end


  it "should be logged out on clicking the 'sign out' link" do
    page.click_link 'Sign out'

    page.should_not have_content 'Sign out'
    page.should have_content 'Goodbye'
    page.should have_content 'Sign in with'
    page.should have_content 'Projects'
  end


  it "should show a list of projects on the projects-list page" do
    page.visit '/projects'
    validate_projects_list TestData.projects
  end

  
  it "should show a list of issues on an accessible project's issues page" do
    page.visit '/projects'
    validate_projects_list TestData.projects

    test_project = TestData.projects.first

    page.click_on test_project[:title]
    validate_project_page test_project
  end

  
  it "should show a title and comments on an issue page" do
    test_project = TestData.projects.first
    page.visit test_project[:url]
    validate_project_page test_project

    test_issue = test_project[:issues].first
    click_on test_issue[:title]
    validate_issue_page test_issue, test_project
  end

  
  it "should add a comment to an issue" do
    test_issue = TestData.projects.first[:issues].first
    page.visit test_issue[:url]
    validate_issue_page test_issue

    comment_text = 'This is my first test comment'
    page.fill_in 'description-text', :with => comment_text
    page.click_button 'Submit'

    validate_issue_page test_issue
    page.should have_content comment_text
  end

  
  it "should create an unassigned issue, and then assign someone to it" do
    test_project = TestData.projects.first

    page.visit test_project[:url]
    validate_project_page test_project

    page.click_on 'New issue'
    find( '.header-sidebar' ).should have_content test_project[:title]
    find( '.header-sidebar' ).should have_content 'New issue'
    page.should have_content 'New issue for project "'
    
    issue_title = 'An important test issue!'
    issue_text = 'Actually, it\'s not that important. This is just my first test issue'
      
    page.fill_in 'issue-title', :with => issue_title
    page.fill_in 'description-text', :with => issue_text
    page.click_button 'Submit'
    
    validate_issue_page( { title:issue_title, comments:[{text:issue_text}] }, test_project )
    
    test_assignee = TestData.users.last
    page.choose test_assignee[:name]
    page.click_button 'Submit'

    page.should have_content "Assigned to: #{test_assignee[:name]}"
    page.should have_content "assigned this issue to #{test_assignee[:name]}"
  end

  
  it "should get an error message when failing to create a new issue " do
    test_project = TestData.projects.first

    page.visit test_project[:url]
    validate_project_page test_project

    page.click_on 'New issue'
    find( '.header-sidebar' ).should have_content test_project[:title]
    find( '.header-sidebar' ).should have_content 'New issue'
    page.should have_content 'New issue for project "'
    
    page.click_button 'Submit'
    
    page.should have_content 'Could not create a new issue.'
    page.should have_content 'These are the errors:'
    page.should have_content 'Title must not be blank'
  end

  
  it "should create a new assigned issue from the project page" do
    test_project = TestData.projects.first

    page.visit test_project[:url]
    validate_project_page test_project

    page.click_on 'New issue'
    find( '.header-sidebar' ).should have_content test_project[:title]
    find( '.header-sidebar' ).should have_content 'New issue'
    page.should have_content 'New issue for project "'
    
    page.fill_in 'issue-title', :with => 'An important test issue!'
    page.fill_in 'description-text', :with => 'Actually, it\'s not that important. This is just my first test issue'
    page.choose 'miranda'
    page.click_button 'Submit'
    
    page.should have_content 'miranda'
    page.should have_content 'One crazy project'
    page.should have_content 'important test issue'
    page.should have_content 'Assigned to: miranda'
    page.should have_content 'first test issue'
  end


  it "should be denied access to a private project when not a member" do
    page.visit '/project/23/issues'
    page.should have_content 'Access denied. 403'
  end

  
  it "should be able to join a public project" do
    page.visit '/projects'
    validate_projects_list TestData.projects

    within '.other-projects' do
      public_projects = page.all( '.public-project' )
      public_projects.count.should eq(1)
      public_projects.first.click_link 'Join'
    end
    
    page.should have_content 'Issue list'
    page.should have_content 'You are now a project member'

    page.visit '/projects'
    find( '.projects-where-active' ).should have_content 'An only mildly crazy project'
  end

  
  it "should be able to join a private project" do
    page.visit '/projects'
    validate_projects_list TestData.projects

    # Click on 'Join' for an inaccessible private project
    within '.other-projects' do
      private_projects = page.all( '.private-project' )
      private_projects.count.should eq(1)
      private_projects.first.click_link 'Join'
    end

    # Submit the join request form
    page.should have_content 'Message for the project members'
    page.fill_in 'message', :with => 'Please, please, let me me join!'
    page.click_button 'Send request'
    validate_projects_list TestData.projects
    page.should have_content 'Your request has been sent to the project members.'

    # projects list should have an extra column for pending memberships
    find( '.projects-where-membership-pending .private-project' ).should have_content 'A most sensible project'
    find( '.projects-where-membership-pending .private-project' ).should_not have_content 'Join'

    # Get membership request issue id so that we can use this in other tests.
    # Note: Issue ID added to UI specifically to make it more testable.
    membership_request_issue_id = /Your request has been sent to the project members. Issue #(\d+)/.match( page.text ).captures.first
    
    # Membership request issue should still be inaccessible to applicant
    page.visit "/issue/#{membership_request_issue_id}"
    page.should have_content 'Access denied'

    # Membership request should visible to an active project member
    page.visit '/logout'
    sign_in 'bertie', 'password_2'
    page.visit "/issue/#{membership_request_issue_id}"
    page.should have_content 'miranda requests project membership'

    # An active project member should be able to accept membership request
    page.click_link 'click here'
    page.should have_content "bertie accepted miranda's membership request"
    page.should have_content "miranda is now a member of the project"

    # The new member should no longer have a pending membership
    page.visit '/logout'
    sign_in 'miranda', 'password_1'
    page.visit '/projects'
    page.should_not have_content 'Membership pending'

    # The new member should be able to view the membership request issue
    page.visit "/issue/#{membership_request_issue_id}"
    page.should have_content 'miranda requests project membership'
    page.should have_content "bertie accepted miranda's membership request"
    page.should have_content "miranda is now a member of the project"
  end
end
