
require 'mail'

require_relative 'support/test_data.rb'
require_relative 'support/integration_test_helpers.rb'


describe "Subscriptions", :type => :feature do
  include IntegrationTestHelpers
  include Mail::Matchers

  before :all do
    Mail.defaults do
      delivery_method :test
    end
  end
    
  before :each do
    # This does not work in before :all
    # Is there some setting needed to preserve the session between tests?
    # Is it as bad idea to have assertions in before :each ?
    sign_in TestData.primary_user[:name], TestData.primary_user[:password]
  end


  it "should be able to subscribe to a project" do
    test_project = TestData.projects.first

    page.visit test_project[:url]
    validate_project_page test_project

    page.click_on 'Subscribe'
    page.should have_content 'You will now receive update notifications for this project.'

    issue_title = 'Testing subscriptions'
    comment1 = 'This is a subscription test.'
    add_issue_to_current_project issue_title, comment1
    
    comment2 = 'This is another subscription test.'
    add_comment_to_current_issue comment2

    should have_sent_email.
      to(TestData.primary_user[:email]).
      matching_subject( /#{issue_title}/ )
  end

end
