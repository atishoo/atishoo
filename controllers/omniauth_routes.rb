require 'sinatra/base'
require 'pp'


class Atishoo < Sinatra::Base
  
  # For any route other than '/create-account', delete account-creation session
  # variables.
  #
  before '*' do | path |
    unless [ '/create-account', '/create-account/' ].include? path
      if session[:auth_stage] == :create_account
        delete_account_creation_session_variables
        flash.warning 'Account creation cancelled'
      end
    end
  end
  

  get '/sign-in/:provider' do
    halt 403 if app_session.logged_in?
    redirect "/auth/#{params[:provider]}"
  end
  
  
  get '/auth/:provider/callback' do
    halt 403 if app_session.logged_in?

    omni = request.env['omniauth.auth']
    user = User.authenticate omni[:provider], omni[:uid]

    if user
      log_in_user user, omni[:provider]
    else
      offer_account_creation omni
    end
  end

  
  get '/auth/failure' do
    halt 403 if app_session.logged_in?
    params.pretty_inspect
  end


  get '/create-account' do
    halt 403 if app_session.logged_in? or session[:auth_stage] != :create_account
    create_account_and_sign_in( session[:provider],
                                session[:external_uid],
                                session[:nickname] )
  end


  get '/test' do

    mustache :create_account, {
      :provider => 'Twitter',
      :nickname => 'JHellings',
      :description => 'Hell of a guy. We would have kept him but you know how it is. Genius like that is always restless. Eh? Oh him! No, I was thinking of someone else.',
      :image => 'http://pbs.twimg.com/profile_images/2299210436/857a2lcuv0d9inir4ime_normal.jpeg'
    }
  end
  
  
  private  ## helper methods

  def log_in_user user, provider
    app_session.log_in user
    
    if app_session.logged_in?
      flash.notice "Welcome"
    else
      flash.error "Something went wrong while trying to log you in through #{provider.capitalize}"
    end
 
    redirect '/'
  end

  
  def offer_account_creation omni_hash
    session[:auth_stage] = :create_account
    session[:provider] = omni_hash['provider']
    session[:external_uid] = omni_hash['uid']
    session[:nickname] = omni_hash['info']['nickname']

    mustache :create_account, {
      :provider => omni_hash['provider'],
      :nickname => omni_hash['info']['nickname'],
      :description => omni_hash['info']['description'],
      :image => omni_hash['info']['image']
    }
  end


  def create_account_and_sign_in provider, external_uid, nickname

    u = User.create_with_authorisation( nickname, provider, external_uid )
    app_session.log_in u
    
    if app_session.logged_in?
      flash.notice "Welcome to your new account"
    else
      flash.error "Something went wrong while trying to create an account for you."
    end
 
    delete_account_creation_session_variables
    redirect '/'
  end


  def delete_account_creation_session_variables
      session.delete :auth_stage
      session.delete :provider
      session.delete :external_uid
      session.delete :nickname
  end
end
