require 'sinatra/base'
require 'views.rb'
require 'models.rb'


class Atishoo < Sinatra::Base
  
  ## Redirects
  ## Precede access tests in order to avoid checking access twice.
  
  before '/project/:project_id' do | project_id |
    redirect "/project/#{project_id}/issues"
  end

  
  ## Access tests
  
  before '/project/:project_id*' do | project_id, sub_path |

    @project = Project.get( project_id )
    halt 404 unless @project
    
    user = app_session.current_user
    
    if sub_path == '/join'
      halt 403 unless user and not user.is_member_of?( @project )
    else
      halt 403 unless @project.accessible_to? user
    end
  end


  ## Routes
  
  get '/project/:project_id/issues' do | project_id |
    mustache :issue_list, :project => @project
  end


  get '/project/:project_id/add-issue' do | project_id |
    halt 403 unless app_session.current_user.is_active_member_of? @project
    mustache :add_issue, :project => @project
  end

  post '/project/:project_id/add-issue' do | project_id |
    user = app_session.current_user
    halt 403 unless user.is_active_member_of? @project
    add_issue( @project, user,
               params[:title], params[:description_text], params[:assignee] )
  end


  get '/project/:project_id/join' do | project_id |

    if @project.private?
      mustache :join_project, :project => @project
    else
      join_public_project @project, app_session.current_user
    end
  end
  
  post '/project/:project_id/join' do | project_id |
    halt 404 unless @project.private?
    request_membership_of_private_project( @project, app_session.current_user )
  end
  

  get '/project/:project_id/accept-request-from/:user_id' do
    | project_id, user_id |

    applicant = User.get(user_id)
    halt 404 unless applicant and applicant.is_pending_member_of? @project

    granter = app_session.current_user
    halt 403 unless granter and granter.is_active_member_of? @project

    convert_pending_membership_to_active( @project, applicant, granter )
  end
  

  get '/project/:project_id/subscribe' do | project_id |
    add_project_subscription @project, app_session.current_user
  end
  
  
  private ## helper methods


  def add_issue project, user, title, text, assignee_id
    
    assignee = User.get( assignee_id ) if assignee_id
    
    if assignee
      text << "<br>--<br>#{user.login} assigned this issue to #{assignee.login}"
    end
    
    i = project.add_issue( notifier, user, title, text, assignee )

    if i.errors.empty?
      flash.notice "Created new issue: #{i.title}"
      redirect "/issue/#{i.id}"

    else
      msg = 'Could not create a new issue. These are the errors:<ul>'
      i.errors.each { |e| e.each { |item| msg += "<li>#{item }</li>" } }
      msg += '</ul>'
      flash.error msg

      mustache :add_issue, :project => @project, :title => title, :main_text => text
    end
  end

  
  def join_public_project project, user
    
    project.add_active_member user
    flash.notice 'You are now a project member.'
    redirect "/project/#{project.id}"
  end
  
  
  def request_membership_of_private_project project, user

    message =
      "#{params[:message]}<br>--<br>To accept this user as a member of the project, just <a href=\"/project/#{project.id}/accept-request-from/#{user.id}\">click here<a>."
    
    pm = project.add_pending_member notifier, user, message

    issue_num = pm.pending_details.issue.id

    # Including Issue ID in order to make app more testable.
    flash.notice "Your request has been sent to the project members. Issue ##{issue_num}"
    redirect '/'
  end

  
  def convert_pending_membership_to_active project, applicant, granter
    
    message =
      "#{granter.login} accepted #{applicant.login}'s membership request.<br>#{applicant.login} is now a member of the project."

    pending_details =
      project.make_pending_membership_active( notifier, applicant, granter, message )

    redirect "/issue/#{pending_details.issue.id}"
  end


  def add_project_subscription project, user

    ps = project.add_subscriber user

    if ps.errors.empty?
      flash.notice 'You will now receive update notifications for this project.'

    else
      msg = 'Could not subscribe you. These are the errors:<ul>'
      ps.errors.each { |e| e.each { |item| msg += "<li>#{item }</li>" } }
      msg += '</ul>'
      flash.error msg
    end

    redirect "/project/#{project.id}"
  end
end
