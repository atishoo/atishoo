require 'sinatra/base'


class Atishoo < Sinatra::Base
  
  before '/issue/:issue_id*' do | issue_id, sub_path |
    @issue = Issue.get( issue_id )
    halt 404 unless @issue
    halt 403 unless @issue.accessible_to? app_session.current_user
  end

  get '/issue/:issue_id' do | issue_id |
    mustache :view_issue, :issue => @issue
  end

  post '/issue/:issue_id/comment/add' do | issue_id |

    user = app_session.current_user
    halt 403 unless user.is_active_member_of? @issue.project
    
    comment_text = params[:description_text]
    assignee_id = params[:assignee]
    
    if assignee_id or not comment_text.empty?
      comment_on_or_assign_issue @issue, user, comment_text, assignee_id
    else
      mustache :view_issue, :issue => @issue
    end
  end

  
  private   ## helper methods

  def comment_on_or_assign_issue issue, user, comment_text, assignee_id

    if comment_text or assignee_id

      text = comment_text || ''
      assignee = User.get( assignee_id )
      
      if assignee
        issue.assign_to assignee
        text << "<br>--<br>" unless text.empty?
        text << "#{user.login} assigned this issue to #{assignee.login}"
      end
      
      issue.add_comment( notifier, user, comment_text )
    end
    
    redirect "/issue/#{issue.id}"
  end
end
