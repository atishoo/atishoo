require 'sinatra/base'
require 'views.rb'
require 'models.rb'


class Atishoo < Sinatra::Base

  ## Redirects
  ## Precede access tests in order to avoid checking access twice.
  
  # Where the user ID is not explicilty given, assume current user's ID.
  #
  before %r{^/user(?!/\d+)(.*)$} do | sub_path |
    halt 403 unless app_session.logged_in?
    redirect "/user/#{app_session.current_user.id}#{sub_path}"
  end

  # Where a specific user-related page is not given, go to profile.
  # TODO: Change redirect from '.../settings' to '.../profile'.
  #
  before '/user/:user_id' do | user_id |
    redirect "/user/#{user_id}/settings"
  end
  
  
  ## Access tests
  
  before '/user/:user_id*' do | user_id, sub_path |

    @user ||= User.get( user_id )
    halt 404 unless @user
    
    # TODO - implement admin permissions to edit other users' settings.
    halt 403 unless @user == app_session.current_user
  end


  ## Routes
  
  get '/user/:user_id/settings' do
    mustache :user_settings, :user => @user
  end
  
  post '/user/:user_id/settings' do
    # halt 403 unless current user is allowed to edit given user's settings
    update_user_settings @user, params
  end

  
  private ## Helpers

  def update_user_settings user, params

    user.email = params[:email]

    if user.save
      flash.notice 'User settings updated'

    else
      msg = 'Could not update user settings. These are the errors:<ul>'
      user.errors.each { |e| e.each { |item| msg += "<li>#{item}</li>" } }
      msg += '</ul>'
      flash.error msg
    end
    
    mustache :user_settings, :user => @user
  end

end
