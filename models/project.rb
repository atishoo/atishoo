
class Project
  include DataMapper::Resource

  STATUS_PRIVATE = 1
  STATUS_PUBLIC = 2
  STATUS_STRINGS = {
    STATUS_PRIVATE => 'private',
    STATUS_PUBLIC => 'public'
  }

  property :id, Serial
  property :friendly_id, String, :required => true, :length => 31
  property :title, String, :required => true, :length => 63

  property :status, Integer, :required => true, :default => STATUS_PRIVATE
  validates_within :status, :set => STATUS_PRIVATE..STATUS_PUBLIC

  has n, :project_memberships
  has n, :members, 'User', :through => :project_memberships 
  has n, :issues
  has n, :project_subscriptions

  def public?
    status == STATUS_PUBLIC
  end

  def private?
    status == STATUS_PRIVATE
  end

  def accessible_to? user
    public? or ( user and user.is_active_member_of? self )
  end

  def status_string
    STATUS_STRINGS[status] || 'unknown'
  end

  def add_issue notifier, user, title, text, assignee = nil
    i = Issue.create( :project => self, :title => title, :assigned_to => assignee )
    i.add_comment( notifier, user, text )
    i
  end

  def add_active_member user
    add_member user, ProjectMembership::STATUS_ACTIVE
  end
  
  def add_pending_member notifier, user, message
    i = add_issue notifier, user, "#{user.login} requests project membership", message
    add_member user, ProjectMembership::STATUS_PENDING, i
  end
  
  def make_pending_membership_active notifier, applicant, granter, acceptance_message
    pm = ProjectMembership.all( :member => applicant, :project => self ).first
    success = pm.set_active
    pm.pending_details.issue.add_comment( notifier, granter, acceptance_message )
    pm.pending_details
  end
  
  def add_subscriber user
    ProjectSubscription.create( :project => self, :user => user )
  end

  
  private
  
  def add_member user, membership_status, issue = nil
    pm = ProjectMembership.create( :project => self,
                                   :member => user,
                                   :status => membership_status )
    if issue
      PendingMembershipDetails.create( :project_membership => pm,
                                       :issue => issue )
    end
    pm
  end

end


class ProjectMembership
  include DataMapper::Resource

  STATUS_PENDING = 1
  STATUS_ACTIVE = 2
  STATUS_BLOCKED = 3

  property :id, Serial

  property :status, Integer, :required => true, :default => STATUS_PENDING
  validates_within :status, :set => STATUS_PENDING..STATUS_BLOCKED

  has 1, :pending_details, 'PendingMembershipDetails'
  
  property :member_id, Integer, :unique_index => :uniqueness, :required => true
  property :project_id, Integer, :unique_index => :uniqueness, :required => true
  
  belongs_to :member, 'User'
  belongs_to :project

  def active?
    status == STATUS_ACTIVE
  end

  def pending?
    status == STATUS_PENDING
  end

  def blocked?
    status == STATUS_BLOCKED
  end

  def set_active
    self.status = STATUS_ACTIVE
    save
  end
end


class PendingMembershipDetails
  include DataMapper::Resource

  property :id, Serial
  
  property :issue_id, Integer, :unique_index => true, :required => true
  
  # Even though we have set the following in ProjectMembership...
  #     has 1, :pending_details, 'PendingMembershipDetails'
  # ...we still need to set unique index here.
  property :project_membership_id, Integer, :unique_index => true, :required => true
  
  belongs_to :issue
  belongs_to :project_membership
end


class ProjectSubscription
  include DataMapper::Resource

  property :id, Serial

  property :user_id, Integer, :unique_index => :uniqueness, :required => true
  property :project_id, Integer, :unique_index => :uniqueness, :required => true
  
  belongs_to :user
  belongs_to :project

  validates_with_block do
    if user and user.valid_email_address?
      true
    elsif not user
      [ false, 'User must be logged in to subscribe.' ]
    elsif not user.valid_email_address?
      [ false, 'User must have a valid email address in order to subscribe.' ]
    end
  end
end
