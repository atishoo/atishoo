require 'dm-core'

class Authorisation
  include DataMapper::Resource

  property :id, Serial
  property :provider, String, :length => 31, :required => true, :unique_index => :auth_index
  property :uid, String, :length => 63, :required => true, :unique_index => :auth_index

  belongs_to :user,  :required =>true
  
end
