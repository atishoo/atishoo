require 'support/html_cleaner.rb'


class Issue
  include DataMapper::Resource

  property :id, Serial
  property :title, String, :length => 127, :required => true

  belongs_to :assigned_to, 'User', :required => false
  belongs_to :project, :required => true
  has n, :issue_comments

  def comments
    issue_comments
  end

  def add_comment notifier, user, text
    c = IssueComment.create( :body => text, :user => user, :issue => self )
    notifier.notify_about_comment c, users_to_notify
  end

  def accessible_to? user
    project.accessible_to? user
  end

  def assign_to user
    self.assigned_to = user
    save
  end

  def users_to_notify
    project.project_subscriptions.map { |s| s.user }
  end
end


class IssueComment
  include DataMapper::Resource

  property :id, Serial
  property :body, Text, :required => true
  
  belongs_to :issue, :required => true
  belongs_to :user, :required => true

  def filtered_body
    HtmlCleaner.allow_basic_html body
  end

  def plain_text_body
    HtmlCleaner.convert_to_plain body
  end
end
