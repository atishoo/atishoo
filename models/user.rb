require 'dm-types'
require 'models/project.rb'


class User
  include DataMapper::Resource

  property :id, Serial
  property :login, String, :length => 63, :required => true
  property :email, String, :length => 63

  has n, :project_memberships, :child_key => [ :member_id ]
  has n, :projects, :through => :project_memberships

  has n, :assigned_issues, 'Issue', :child_key => [ :assigned_to_id ]

  has n, :issue_comments
  has n, :issues, :through => :issue_comments

  has 1, :admin_user_details
  has n, :authorisations
  
  has n, :project_subscriptions

  
  def discussed_issues
    issues
  end


  def may_subscribe?
    valid_email_address?
  end

  def is_subscribed_to? project
    project_subscriptions.any? { |s| s.project == project }
  end

  def valid_email_address?
    !! email
  end
  
  
  def is_member_of? project
    projects.include? project
  end
  
  def is_active_member_of? project
    pm = ProjectMembership.all( :member => self, :project => project ).first
    pm and pm.active?
  end
  
  def is_pending_member_of? project
    pm = ProjectMembership.all( :member => self, :project => project ).first
    pm and pm.pending?
  end

  def active_memberships
    project_memberships.select { | pm | pm.active? }
  end

  def pending_memberships
    project_memberships.select { | pm | pm.pending? }
  end

  
  def self.authenticate provider, uid
    a = Authorisation.all( provider: provider, uid: uid ).first
    a.user if a
  end

  def self.authenticate_admin username, password
    user = all( :login => username ).first
    if user && user.admin_user_details.password == password
      user
    else
      nil
    end
  end

  def self.create_with_authorisation nickname, provider, uid
    u = create( :login => get_unique_login( nickname ) )
    a = Authorisation.create( :provider => provider,
                              :uid => uid,
                              :user => u )
    u
  end

  
  private

  LOGIN_GENERATION_LIMIT = 100
  
  def self.get_unique_login nickname
    login_stem = nickname.downcase
    login_suffix = 1
    login = login_stem
    
    while not all( :login => login ).empty?
      
      login = "#{login_stem}#{login_suffix}"
      login_suffix += 1

      if login_suffix > LOGIN_GENERATION_LIMIT 
        raise "Can't find a unique login name after #{LOGIN_GENERATION_LIMIT } attempts!"
      end
    end
    login
  end
end


class AdminUserDetails
  include DataMapper::Resource

  property :id, Serial
  property :password, BCryptHash

  belongs_to :user, :required => true
end
