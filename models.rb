
# Was requiring DM gems in individual model files but crashes occurred when
# only some models could 'see' dm-validations.

require 'dm-core'
require 'dm-validations'



require 'models/user.rb'
require 'models/project.rb'
require 'models/issue.rb'
require 'models/authorisation.rb'
