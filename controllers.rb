#
# controllers.rb
#
# All controllers for the Atishoo app.
#

require 'sinatra/base'
require 'models.rb'
require 'views.rb'


require 'controllers/project_routes.rb'
require 'controllers/issue_routes.rb'
require 'controllers/omniauth_routes.rb'
require 'controllers/user_routes.rb'
  

class Atishoo < Sinatra::Base

  # Let's not force ourselves to make trailing slash optional on every route.
  #
  before '/*/' do | path |
    redirect '/' + path
  end


  get '/' do
    redirect '/projects' if app_session.logged_in?
    mustache :logged_out_home
  end
  
  get '/projects' do
    redirect '/' unless app_session.logged_in?
    mustache :project_list
  end

  
  get '/admin/login' do
    halt 403 if app_session.logged_in?
    mustache :admin_login
  end
  
  post '/admin/login' do
    halt 403 if app_session.logged_in?
      
    app_session.log_in( User.authenticate_admin params[:username], params[:password] )
    
    if app_session.logged_in?
      flash.notice "Welcome"
      redirect '/'
    else
      flash.error "Incorrect username or password"
      redirect '/admin/login'
    end
  end
  

  get '/logout' do
    app_session.log_out
    flash.notice 'Goodbye'
    redirect '/'
  end
  

  error 403 do
    "Access denied. 403"
  end

  not_found do
    "Not found. 404"
  end

  error do
    "<h2>Uncaught exception</h2>params:<p>#{params}</p>request:<p>#{request}</p>"
  end
end


